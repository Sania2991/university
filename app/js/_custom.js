document.addEventListener("DOMContentLoaded", function () {
	$(document).ready(function () {

		var slide = $('#sliderStudentLife'),
				ctrCurrentSlide = $('#controls .currentSlide'),
				ctrTotalSlide = $('#controls .totalSlide'),
				ctrPrevSlide = $('#goToPrevSlide'),
				ctrNextSlide = $('#goToNextSlide');

		// Наши преподаватели
		var sliderStudentLife = slide.lightSlider({
			auto: true,
			// adaptiveHeight: true,
			pauseOnHover: true,
			pause: 7000,
			loop: true,
			item: 1,
			slideMove: 1,
			slideMargin: 50,
			easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
			speed: 1000,
			controls: false,
			responsive: [{
				breakpoint: 1079,
				settings: {
					slideMargin: 20,
				}
			},{
				breakpoint: 766,
				settings: {
					slideMargin: 10,
				}
			}],
			onBeforeStart: function (el) {
				ctrTotalSlide.text(slide.length);
				$('#sliderStudentLife').removeClass('cs-hidden');
			},
			onBeforeSlide: function (el) {
				let currentPage = el.getCurrentSlideCount();

				if (currentPage == 1) {
					ctrNextSlide.removeClass('unable').prop("disabled", false);
					ctrPrevSlide.addClass('unable').prop("disabled", true);
				} else if (currentPage == sliderStudentLife.getTotalSlideCount()){
					ctrPrevSlide.removeClass('unable').prop("disabled", false);
					ctrNextSlide.addClass('unable').prop("disabled", true);
				} else {
					ctrPrevSlide.removeClass('unable').prop("disabled", false);
					ctrNextSlide.removeClass('unable').prop("disabled", false);
				};
				ctrCurrentSlide.text(currentPage);
			}
		});

		// первоначальные действия
		ctrCurrentSlide.text(sliderStudentLife.getCurrentSlideCount());
		ctrTotalSlide.text(sliderStudentLife.getTotalSlideCount());

		// нажатие кнопок переходов слайла
		ctrPrevSlide.on('click', function (e) {
			e.preventDefault();
			sliderStudentLife.goToPrevSlide();
		});
		ctrNextSlide.on('click', function (e) {
			e.preventDefault();
			sliderStudentLife.goToNextSlide();
		});



		// VIDEO
		// var videoBlock = document.getElementById('video-block'),
		// 		// vid = document.getElementById('vid');
		// 		vid = document._video;


		// НА МОБИЛЬНЫХ
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			// console.log('мобильное устройство');
		} else {
			$('.k-slide__media video').closest('.k-slide__media').addClass('paused');

			$('.k-slide__media').on('click', function(e) {
				e.preventDefault();
				let videoBlock = $(this).closest('.k-slide__media');
				console.log(videoBlock);
				let vid = videoBlock.find('video')
				console.log(vid);
				if (vid.get(0).paused) {
					vid.get(0).play();
					videoBlock.removeClass('paused');
				} else {
					vid.get(0).pause();
					videoBlock.addClass('paused');
				};
			})
		}

	});
});