// ПОРЯДОК ДЕЙСТВИЙ:

// Добавить свои данные:
// gulp.rsync: добаить свои настройки сервера вместо FIXME
// gulp.scripts:  выбрать какой jQuery используем?



// КОМАНДЫ
// $ gulp 	-	default
// $ gulp styles  -  добавление и минификация стилей
// $ gulp scripts  -  добавление и минификация всех скриптов
// $ gulp watch  -  следит за файлами и обновляет
// !!! gulp.rsync 	-  отправка на сервера (нужно изменить данные)
// $ gulp host	-	 заливаем на сервер (включая gulpfile.js и readme.md)


// Подзадачи (вспомогательные):
// $ gulp browser-sync 	- запуск лок. сервера
// $ gulp settings  -  делает копию файлов "readme.md" и "gulpfile.js"  в папку проекта "option", чтобы на сервере был весь проект, на всякий случай



// *** ПЕРЕМЕННЫЕ ***
var syntax        = 'sass', // Syntax: sass or scss;
		gulpversion   = '4'; // Gulp version: 3 or 4

if (gulpversion == 4) {  }


var syntax        = 'sass', // Syntax: sass or scss;
		gulpversion   = '4'; // Gulp version: 3 or 4


var gulp         = require('gulp'),
		sass         = require('gulp-sass'),
		browserSync  = require('browser-sync'),
		concat       = require('gulp-concat'),
		uglify       = require('gulp-uglify-es').default,
		cleancss     = require('gulp-clean-css'),
		autoprefixer = require('gulp-autoprefixer'),
		rsync        = require('gulp-rsync'),
		newer        = require('gulp-newer'),
		rename       = require('gulp-rename'),
		responsive   = require('gulp-responsive'),
		del          = require('del');
	  // из font-size, font-family => font:
		// ОПАСНО может быть!
	  // shorthand = require('gulp-shorthand')


// Local Server
gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false,
		// open: false,		// перезагрузка, убрать, если нужно каждый раз перегружать сервер
		// online: false, // Work offline without internet connection
		// tunnel: true, tunnel: 'projectname', // Demonstration page: http://projectname.localtunnel.me
	})
});
function bsReload(done) { browserSync.reload(); done(); };





//  ███───███───██─██───█─────███───███
//  █──────█─────███────█─────█─────█
//  ███────█──────█─────█─────███───███
//  ──█────█──────█─────█─────█───────█
//  ███────█──────█─────███───███───███
// $style

gulp.task('styles', function() {
	return gulp.src('app/sass/**/*.sass')
	.pipe(sass({ outputStyle: 'expanded' }))
	.pipe(concat('styles.min.css'))
	.pipe(autoprefixer({
		grid: true,
		overrideBrowserslist: ['last 10 versions']
	}))
	.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Optional. Comment out when debugging
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.stream())
});





//  ███───███───████───████───███────────────██───███
//  █──────█────█──█───█──█────█──────────────█───█
//  ███────█────████───████────█─────███──────█───███
//  ──█────█────█──█───█─█─────█───────────█──█─────█
//  ███────█────█──█───█─█─────█───────────████───███
//  $js

gulp.task('start-scripts', function() {
  return gulp.src([
    // ***** jQuery *****
    // • используем либо 'npm' для самой последней версии
    // 'node_modules/jquery/dist/jquery.min.js', // Optional jQuery plug-in (npm i --save-dev jquery)
    // • либо готовую из 'libs'
    'app/libs/jquery/dist/jquery.min.js',
    // • либо для WordPress переопределяем в function.php и добавляем свою

    // Поддержка старых браузеров для svg
    // $fix-SVG-1
    //'app/libs/svgo/dist/svg4everybody.js',    // поддержка svg для старых браузеров


    // Галлереи/слайдера/сортировка
    'app/libs/lightslider-master/dist/js/lightslider.min.js',  // LightSlider
    // 'app/libs/lightGallery-master/dist/js/lightgallery-all.min.js',  // LightGallery
    // 'app/libs/fotorama/fotorama.js', // Fotorama  -  галлерея с маленькими демками
    // 'app/libs/isotope-docs/js/isotope-docs.min.js',  // Isotop - красивая сортировка элементов
    // 'app/libs/fancybox-master/dist/jquery.fancybox.min.js', // FancyBox  -  увеличение фото + галлерея


    // Доп. функционал
    // FIXME: 'app/libs/clamp.js/clamp.js',   // Clamp  -  ограничивает длину, ставит троеточие (не больше 2-ух строк)


    // 'app/js/_lazy.js', // JS library plug-in example

    ])
  .pipe(concat('start-scripts.min.js'))
  .pipe(uglify()) // Minify js (opt.)
  .pipe(gulp.dest('app/js'))
  .pipe(browserSync.reload({ stream: true }))
});



//    ██───███
//     █───█
//     █───███
//  █──█─────█
//  ████───███

gulp.task('scripts', function() {
  return gulp.src([
    'app/js/_custom.js', // Custom scripts. Always at the end
  ])
.pipe(concat('scripts.min.js'))
.pipe(uglify()) // Minify js (opt.)
.pipe(gulp.dest('app/js'))
.pipe(browserSync.reload({ stream: true }))
}); 




// Code & Reload
gulp.task('code', function() {
	return gulp.src('app/**/*.html')
	.pipe(browserSync.reload({ stream: true }))
});


// добавил специально, чтобы на сервере хранился весь проект (на всякий случай)
gulp.task('settings', function() {
	return gulp.src([
		'gulpfile.js',
		'readme.md'
		])
	.pipe(gulp.dest('app/options/'))
});





//  ████────███───████───█─────████───██─██
//  █──██───█─────█──█───█─────█──█────███
//  █──██───███───████───█─────█──█─────█
//  █──██───█─────█──────█─────█──█─────█
//  ████────███───█──────███───████─────█
// $deploy

// Отправка папки rsync
gulp.task('rsync', function() {
	return gulp.src('app/')
	.pipe(rsync({
		root: 'app/',
		// FIXME - специально, чтобы не сбить мои настройки
    hostname: 'vh264@isp1.ru.fastfox.pro',
    destination: 'www/vh264.ffox.site/',
		// include: ['*.htaccess'], // Included files
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excluded files
		recursive: true,	// рекурсивно (включая вложенные)
		archive: true,
		silent: false,
		compress: true,
		emptyDirectories: true,   // пусты каталоги
		clean: true,  // удалять?
		//progress: true,   // показываем прогресс
	}))
});





//  █───█───████───███───████───█──█
//  █───█───█──█────█────█──█───█──█
//  █─█─█───████────█────█──────████
//  █████───█──█────█────█──█───█──█
//  ─█─█────█──█────█────████───█──█
// $watch

gulp.task('watch', function() {
	gulp.watch('app/sass/**/*.sass', gulp.parallel('styles'));
	gulp.watch(['libs/**/*.js', 'app/js/_custom.js'], gulp.parallel('scripts'));
	gulp.watch('app/*.html', gulp.parallel('code'));
	// gulp.watch('app/img/_src/**/*', gulp.parallel('img'));
	// следим за изменениями (те же, что и в самой задаче + ". ['имя задачи")
	// gulp.watch(["app/**/*", "!app/libs/**/*"], ['rsync'])
});




//  ███───████───███───█──█───██───███
//  ─█────█──█───█─────█─█────█────█
//  ─█────████───███───██──────────███
//  ─█────█──█─────█───█─█───────────█
//  ─█────█──█───███───█──█────────███


// *****************************************
// $DEFAULT
// *****************************************
// для сервера
// gulp.task('default', gulp.parallel('styles', 'scripts', 'watch'));

// для ЛОКАЛКИ
// без изображение
gulp.task('default', gulp.parallel('styles', 'scripts', 'browser-sync', 'watch'));


// Отправка на сервер
gulp.task('host', gulp.series('settings', 'rsync'));
// Deploy with advanced params
// gulp.task('host', gulp.series('settings', 'rsync', function(){}));